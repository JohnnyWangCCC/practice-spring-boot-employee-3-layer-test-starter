package com.afs.restapi;

import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.service.CompanyService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CompanyControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    CompanyService companyService;

    @Autowired
    CompanyRepository companyRepository;

    ObjectMapper mapper = new ObjectMapper();

    @BeforeEach
    void setUp() {
        companyRepository.clearAll();
    }

    @Test
    void should_get_all_companies_when_perform_get_given_company() throws Exception {
        //given
        Company company = buildCompanyOne();
        companyService.saveCompany(company);

        //when

        //then
        mockMvc.perform(MockMvcRequestBuilders.get("/companies"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].companyName").isString())
                .andExpect(jsonPath("$[0].companyName").value("CompanyOne"))
                .andExpect(jsonPath("$[0].employees", hasSize(1)))
                .andExpect(jsonPath("$[0].employees[0].id").isNumber())
                .andExpect(jsonPath("$[0].employees[0].id").value(1))
                .andExpect(jsonPath("$[0].employees[0].name").isString())
                .andExpect(jsonPath("$[0].employees[0].name").value("personOne"))
                .andExpect(jsonPath("$[0].employees[0].age").isNumber())
                .andExpect(jsonPath("$[0].employees[0].age").value(21))
                .andExpect(jsonPath("$[0].employees[0].gender").isString())
                .andExpect(jsonPath("$[0].employees[0].gender").value("female"))
                .andExpect(jsonPath("$[0].employees[0].salary").isNumber())
                .andExpect(jsonPath("$[0].employees[0].salary").value(6000));
    }

    @Test
    void should_return_company_one_with_id_1_when_perform_get_given_company_id() throws Exception {
        //given
        Company company = buildCompanyOne();
        companyService.saveCompany(company);

        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.companyName").isString())
                .andExpect(jsonPath("$.companyName").value("CompanyOne"))
                .andExpect(jsonPath("$.employees", hasSize(1)))
                .andExpect(jsonPath("$.employees[0].id").isNumber())
                .andExpect(jsonPath("$.employees[0].id").value(1))
                .andExpect(jsonPath("$.employees[0].name").isString())
                .andExpect(jsonPath("$.employees[0].name").value("personOne"))
                .andExpect(jsonPath("$.employees[0].age").isNumber())
                .andExpect(jsonPath("$.employees[0].age").value(21))
                .andExpect(jsonPath("$.employees[0].gender").isString())
                .andExpect(jsonPath("$.employees[0].gender").value("female"))
                .andExpect(jsonPath("$.employees[0].salary").isNumber())
                .andExpect(jsonPath("$.employees[0].salary").value(6000));
    }

    private Company buildCompanyOne() {
        List<Employee> employeesInOne = new ArrayList<>();
        employeesInOne.add(new Employee(1, "personOne", 21, "female", 6000));
        return new Company(1, "CompanyOne", employeesInOne);
    }

    private Company buildCompanyTwo() {
        List<Employee> employeesInTwo = new ArrayList<>();
        employeesInTwo.add(new Employee(1, "personOne", 21, "female", 6000));
        return new Company(2, "CompanyTwo", employeesInTwo);
    }

    @Test
    void should_return_company_one_employees_when_perform_get_given_company_id() throws Exception {
        //given
        Company company = buildCompanyOne();
        companyService.saveCompany(company);

        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/1/employees"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].name").isString())
                .andExpect(jsonPath("$[0].name").value("personOne"))
                .andExpect(jsonPath("$[0].age").isNumber())
                .andExpect(jsonPath("$[0].age").value(21))
                .andExpect(jsonPath("$[0].gender").isString())
                .andExpect(jsonPath("$[0].gender").value("female"))
                .andExpect(jsonPath("$[0].salary").isNumber())
                .andExpect(jsonPath("$[0].salary").value(6000));
    }

    @Test
    void should_return_one_page_companies_when_perform_get_by_page_given_companies() throws Exception {
        //given
        Company companyOne = buildCompanyOne();
        Company companyTwo = buildCompanyTwo();
        companyService.saveCompany(companyOne);
        companyService.saveCompany(companyTwo);
        //when

        //then
        mockMvc.perform(MockMvcRequestBuilders.get("/companies")
                        .param("pageIndex", "1")
                        .param("pageSize", "1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].companyName").isString())
                .andExpect(jsonPath("$[0].companyName").value("CompanyOne"))
                .andExpect(jsonPath("$[0].employees", hasSize(1)))
                .andExpect(jsonPath("$[0].employees[0].id").isNumber())
                .andExpect(jsonPath("$[0].employees[0].id").value(1))
                .andExpect(jsonPath("$[0].employees[0].name").isString())
                .andExpect(jsonPath("$[0].employees[0].name").value("personOne"))
                .andExpect(jsonPath("$[0].employees[0].age").isNumber())
                .andExpect(jsonPath("$[0].employees[0].age").value(21))
                .andExpect(jsonPath("$[0].employees[0].gender").isString())
                .andExpect(jsonPath("$[0].employees[0].gender").value("female"))
                .andExpect(jsonPath("$[0].employees[0].salary").isNumber())
                .andExpect(jsonPath("$[0].employees[0].salary").value(6000));
    }

    @Test
    void should_return_company_save_with_id_when_perform_post_given_a_company() throws Exception {
        //given
        Company company = buildCompanyOne();
        String companyJson = mapper.writeValueAsString(company);

        //when

        //then
        mockMvc.perform(MockMvcRequestBuilders.post("/companies")
                .contentType(MediaType.APPLICATION_JSON)
                .content(companyJson));
    }

    @Test
    void should_update_company_when_perform_put_by_id_given_company_and_update_info() throws Exception {
        //given
        Company company = buildCompanyOne();
        companyService.saveCompany(company);
        Company toBeUpdatecompany = buildCompanyOne();
        toBeUpdatecompany.setCompanyName("Company1");
        String companyJson = mapper.writeValueAsString(toBeUpdatecompany);

        //when then
        mockMvc.perform(MockMvcRequestBuilders.put("/companies/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(companyJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.companyName").isString())
                .andExpect(jsonPath("$.companyName").value("Company1"))
                .andExpect(jsonPath("$.employees", hasSize(1)))
                .andExpect(jsonPath("$.employees[0].id").isNumber())
                .andExpect(jsonPath("$.employees[0].id").value(1))
                .andExpect(jsonPath("$.employees[0].name").isString())
                .andExpect(jsonPath("$.employees[0].name").value("personOne"))
                .andExpect(jsonPath("$.employees[0].age").isNumber())
                .andExpect(jsonPath("$.employees[0].age").value(21))
                .andExpect(jsonPath("$.employees[0].gender").isString())
                .andExpect(jsonPath("$.employees[0].gender").value("female"))
                .andExpect(jsonPath("$.employees[0].salary").isNumber())
                .andExpect(jsonPath("$.employees[0].salary").value(6000));
    }

    @Test
    void should_del_company_when_perform_del_by_id_given_company() throws Exception {
        //given
        Company company = buildCompanyOne();
        companyService.saveCompany(company);

        //when
        mockMvc.perform(MockMvcRequestBuilders.delete("/companies/{id}", 1))
                .andExpect(status().isNoContent());

        //then
        assertThrows(CompanyNotFoundException.class, () -> companyService.getCompany(1));
    }
}