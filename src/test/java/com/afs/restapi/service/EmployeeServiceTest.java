package com.afs.restapi.service;

import com.afs.restapi.exception.EmployeeAgeErrorException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
@AutoConfigureMockMvc
class EmployeeServiceTest {
    private final EmployeeRepository employeeRepository = mock(EmployeeRepository.class);

    private final EmployeeService employeeService = new EmployeeService(employeeRepository);

    @Test
    void should_thrown_age_error_exception_when_insert_employee_given_employee_age15_and_age66() {
        //given
        Employee employeeYoung = new Employee(1, "Xiaowang", 15, "Male", 1000);
        Employee employeeOld = new Employee(1, "Laoli", 66, "Male", 5000);

        //when then
        assertThrows(EmployeeAgeErrorException.class, () -> employeeService.saveEmployee(employeeYoung));
        assertThrows(EmployeeAgeErrorException.class, () -> employeeService.saveEmployee(employeeOld));

    }

    @Test
    void should_not_call_repo_when_save_employee_given_employee_age_not_match() {
        //given
        Employee employeeYoung = new Employee(1, "Xiaowang", 15, "Male", 1000);
        Employee employeeOld = new Employee(1, "Laoli", 66, "Male", 5000);

        //when then
        assertThrows(EmployeeAgeErrorException.class, () -> employeeService.saveEmployee(employeeYoung));
        assertThrows(EmployeeAgeErrorException.class, () -> employeeService.saveEmployee(employeeOld));

        Mockito.verify(employeeRepository, times(0)).insert(any());
    }

    @Test
    void should_throw_not_create_exception_when_save_employee_given_employee_age_35_salary_15000() {
        //given
        Employee employee = new Employee(1, "Laoli", 35, "Male", 15000);

        //when then
        assertThrows(AgeNotMatchSalaryException.class, () -> employeeService.saveEmployee(employee));

        Mockito.verify(employeeRepository, times(0)).insert(any());
    }

    @Test
    void should_return_employee_with_status_true_when_save_employee_given_a_employee_match_rule() {
        //given
        Employee employee = new Employee(1, "Laowang", 38, "Male", 35000);
        Employee employeeToReturn = new Employee(1, "Laowang", 38, "Male", 35000);
        employeeToReturn.setStatus(true);
        when(employeeRepository.insert(any())).thenReturn(employeeToReturn);

        //when
        Employee employeeSaved = employeeService.saveEmployee(employee);

        //then
        assertTrue(employeeSaved.isStatus());
        verify(employeeRepository).insert(argThat(employeeToSave -> {
            assertTrue(employeeToSave.isStatus());
            return true;
        }));
    }

    @Test
    void should_set_status_false_when_delete_employee_given_a_employee_match_rule() {
        //given
        Employee employee = new Employee(1, "Laowang", 38, "Male", 35000, true);
        when(employeeRepository.findById(anyInt())).thenReturn(employee);
        //when
        employeeService.deleteEmployeeById(employee.getId());

        //then
        assertFalse(employee.isStatus());
    }

    @Test
    void should_not_update_when_update_employee_given_a_employee_left() {
        //given
        Employee employee = new Employee(1, "Laowang", 38, "Male", 35000, false);
        when(employeeRepository.update(anyInt(), any())).thenReturn(employee);
        //when
        Employee employeeUpdate = employeeService.updateEmployeeData(1, employee);

        //then
        assertNull(employeeUpdate);
    }

}