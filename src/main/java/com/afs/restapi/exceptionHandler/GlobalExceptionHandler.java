package com.afs.restapi.exceptionHandler;

import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.exception.EmployeeNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(EmployeeNotFoundException.class)
    public ResponseMsg employeeNotFoundExceptionHandler(EmployeeNotFoundException employeeNotFoundException){
        return new ResponseMsg(employeeNotFoundException.getMessage(), HttpStatus.NOT_FOUND.value());
    }

    @ExceptionHandler(CompanyNotFoundException.class)
    public ResponseMsg companyNotFoundExceptionHandler(CompanyNotFoundException companyNotFoundException){
        return new ResponseMsg(companyNotFoundException.getMessage(), HttpStatus.NOT_FOUND.value());
    }
}
