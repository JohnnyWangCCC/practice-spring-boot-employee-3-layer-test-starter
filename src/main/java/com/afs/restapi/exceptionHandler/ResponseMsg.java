package com.afs.restapi.exceptionHandler;

public class ResponseMsg {
    private String message;

    private int code;

    public ResponseMsg() {
    }

    public ResponseMsg(String message, int code) {
        this.message = message;
        this.code = code;
    }
}
