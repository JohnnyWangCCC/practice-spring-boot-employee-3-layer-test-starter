package com.afs.restapi.service;

import com.afs.restapi.exception.EmployeeAgeErrorException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> getEmployeeList() {
        return employeeRepository.findAll();
    }

    public Employee getEmployee(int id) {
        Employee employeeRepositoryById = employeeRepository.findById(id);
        return employeeRepositoryById.isStatus() ? employeeRepositoryById : null;
    }

    public List<Employee> getEmployeesByGender(String gender) {
        return employeeRepository.findByGender(gender);
    }

    public List<Employee> getEmployeesPageQuery(int pageNumber, int pageSize) {
        return employeeRepository.findByPage(pageNumber, pageSize);
    }

    public Employee saveEmployee(Employee employee) {
        if (employee.matchAge()){
            throw new EmployeeAgeErrorException();
        }
        if (employee.matchSalaryWithAge()){
            throw new AgeNotMatchSalaryException();
        }
        employee.setStatus(true);
        return employeeRepository.insert(employee);
    }
    public Employee updateEmployeeData(int id, Employee employee) {
        Employee employeeRepositoryById = employeeRepository.findById(id);
        if (!employee.isStatus()) {
            return null;
        }
        return employeeRepository.update(id, employee);
    }

    public void deleteEmployeeById(int id) {
        Employee employeeRepositoryById = employeeRepository.findById(id);
        if (employeeRepositoryById != null){
            employeeRepositoryById.setStatus(false);
            employeeRepository.update(id, employeeRepositoryById);
        }
    }
}