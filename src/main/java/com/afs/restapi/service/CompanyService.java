package com.afs.restapi.service;

import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.CompanyRepository;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CompanyService {
    private CompanyRepository companyRepository;

    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public void setCompanyRepository(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public List<Company> getCompanyList() {
        return companyRepository.getCompanies();
    }

    public Company getCompany(Integer companyId) {
        return companyRepository.getCompanies().stream()
                .filter(company -> company.getId().equals(companyId))
                .findFirst()
                .orElseThrow(CompanyNotFoundException::new);
    }

    public List<Employee> getEmployees(Integer companyId) {
        return companyRepository.getCompanies().stream()
                .filter(company -> company.getId().equals(companyId))
                .findFirst()
                .map(company -> company.getEmployees())
                .orElse(Collections.emptyList());
    }

    public List<Company> getCompaniesPageQuery(Integer pageIndex, Integer pageSize) {
        return companyRepository.getCompanies().stream()
                .skip((long) (pageIndex - 1) * pageSize)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    public Company updateCompanyData(Integer companyId, Company company) {
        return companyRepository.getCompanies().stream()
                .filter(storedCompany -> storedCompany.getId().equals(companyId))
                .findFirst()
                .map(storedCompany -> updateCompanyAttributes(storedCompany, company))
                .orElse(null);
    }

    private Company updateCompanyAttributes(Company companyStored, Company company) {
        if (company.getCompanyName() != null) {
            companyStored.setCompanyName(company.getCompanyName());
        }
        return companyStored;
    }

    public boolean removeCompany(Integer companyId) {
        return companyRepository.getCompanies().removeIf(company -> company.getId().equals(companyId));
    }

    public void saveCompany(Company company) {
        companyRepository.addCompany(company);
    }
}
