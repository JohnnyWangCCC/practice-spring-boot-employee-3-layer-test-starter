### O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

​	Today we had our usual Code Review and Stand Meeting, and then in the morning we practiced springboot integration testing. In the afternoon, we continued to study yesterday's case, through the explanation of mvc architecture, refactoring, learning the idea of refactoring methods. Then in this case, we also followed the TDD development approach to contact. It is worth noting that in the new requirements, in the modification phase of the integration test, I met a very big trouble, when I modify the new requirements, due to the change of requirements, the integration test also needs a lot of modification, as well as the special characteristics of Springboot, also need us to take extra consideration, which leads to modify the cost of testing more than 50 times greater than that of the requirements of the development, which is also the biggest problem for me.

### R (Reflective): Please use one word to express your feelings about today's class.

​	Bustling.

### I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

​	The most meaningful thing about this exercise is that it gave me a first-hand experience of the differences and difficulties between integration testing and unit testing in web development, and of the difficulties of testing changes when requirements change.

### D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

​	Testing is very important, and how to go about developing the business so that the cost of testing changes can be minimized is a point of great interest, and that's what I'm trying to figure out.